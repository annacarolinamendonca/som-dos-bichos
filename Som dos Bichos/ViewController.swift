//
//  ViewController.swift
//  Som dos Bichos
//
//  Created by Anna Carolina Ribeiro Mendonça on 12/08/19.
//  Copyright © 2019 Anna Carolina Ribeiro Mendonça. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var player = AVAudioPlayer()
    
    @IBAction func cachorro(_ sender: Any) {
        self.executarSom(nomeSom: "cao")
    }
    
    @IBAction func gato(_ sender: Any) {
        self.executarSom(nomeSom: "gato")
    }
    
    @IBAction func leao(_ sender: Any) {
        self.executarSom(nomeSom: "leao")
    }
    
    @IBAction func macaco(_ sender: Any) {
        self.executarSom(nomeSom: "macaco")
    }
    
    @IBAction func vaca(_ sender: Any) {
        self.executarSom(nomeSom: "vaca")
    }
    
    @IBAction func ovelha(_ sender: Any) {
        self.executarSom(nomeSom: "ovelha")
    }
    
    func executarSom(nomeSom: String)  {
        
        if let path = Bundle.main.path(forResource: nomeSom, ofType: "mp3"){
        let url = URL(fileURLWithPath: path)
        
            
            do{
            player = try AVAudioPlayer(contentsOf: url)
                player.prepareToPlay()
                player.play()
                
            }catch{
                print("Erro ao executar o audio")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

